<?php
    $msg_box = ""; // в этой переменной будем хранить сообщения формы
    $errors = array(); // контейнер для ошибок
    // проверяем корректность полей

    $user_name  = $_POST['user_name'];
    $user_email = $_POST['user_email'];
    $text_comment = $_POST['text_comment'];

    if($user_name == "")    $errors[] = "Поле 'Ваше имя' не заполнено!";
    if($user_email == "")   $errors[] = "Поле 'Ваш e-mail' не заполнено!";
    if($text_comment == "") $errors[] = "Поле 'Текст сообщения' не заполнено!";
 
    // если форма без ошибок
    if(empty($errors)){     
        require_once 'connection.php';

        $sql = mysqli_query($connection, "INSERT INTO `comments` (`name`, `mail`, `text`) VALUES ('{$user_name}', '{$user_email}', '{$text_comment}')");
        
        if (!$sql) {
            $msg_box .= "<span style='color: red;'>Ошибка при добавлении в БД</span><br/>";
        }else{
            $msg_box .= "<span style='color: green;'>Спасибо за Ваш отзыв!</span><br/>";
        }
    }else{
        // если были ошибки, то выводим их
        $msg_box = "";
        foreach($errors as $one_error){
            $msg_box .= "<span style='color: red;'>$one_error</span><br/>";
        }
    }
    require_once 'load-comments.php';
 
    // делаем ответ на клиентскую часть в формате JSON
    echo json_encode(array(
        'result' => $msg_box,
        'comments' => $comments
    ));
     
    
?>


<?php 
    mysqli_close($connection);
 ?>