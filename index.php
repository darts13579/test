<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Кодировка веб-страницы -->
    <meta charset="utf-8">
    <!-- Настройка viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Страница отзывов</title>
    <!-- Подключаем Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/site.css" >
	<!-- Подключаем jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Подключаем плагин Popper (необходим для работы компонента Dropdown и др.) -->
    <script src="js/popper.min.js"></script>
    <!-- Подключаем Bootstrap JS -->    
    <script src="js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $('#btn_submit').click(function(){
                // собираем данные с формы
                var user_name    = $('#user_name').val();
                var user_email   = $('#user_email').val();
                var text_comment = $('#text_comment').val();
                // отправляем данные
                $.ajax({
                    url: "add-comment.php", // куда отправляем
                    type: "post", // метод передачи
                    dataType: "json", // тип передачи данных
                    data: { // что отправляем
                        "user_name":    user_name,
                        "user_email":   user_email,
                        "text_comment": text_comment
                    },
                    // после получения ответа сервера
                    success: function(data){
                    	$('.all-comments').html(data.comments);
                        $('.messages').html(data.result);
                    }
                });
            });
        });
    </script>
</head>
<body>
    <div class="page-header">
      <h1>Добро пожаловать на страницу отзывов!</h1>
    </div>
    <!-- Контент страницы -->
    <div class="container-fluid all-comments">
    	<?php 
    		require_once 'load-comments.php';
    		echo $comments;
    	 ?>
    </div>
	
	<div class="col-md-10 comment-form">
		<form action="add_comment.php" method="POST" role="form" class="col-md-10">
			<legend>Оставьте свой отзыв!</legend>
		
			<div class="form-group">
				
			    <div class="messages"></div>
			    <br/>
			    <label>Ваше имя:</label><br/>
			    <input type="text" id="user_name" value="" /><br/>
			     
			    <label>Ваш e-mail:</label><br/>
			    <input type="text" id="user_email" value="" /><br/>
			     
			    <label>Текст сообщения:</label><br/>
			    <textarea id="text_comment" rows="6"></textarea>
			     
			    <br/>
			    
			</div>
		
			<input type="button" value="Отправить" id="btn_submit" class="btn btn-primary" />
		</form>
	</div>
    <footer><div>  </div></footer>
    
</body>
</html>







<?php 
	mysqli_close($connection);
 ?>