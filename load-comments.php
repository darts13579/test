<?php  
	require_once 'connection.php';

	$result = mysqli_query($connection, "SELECT * FROM `comments`");

	if (mysqli_num_rows($result) == 0)
	{
		echo "Записей не найдено";
	} else
	{
		$comments = "";
		while( ($cat = mysqli_fetch_assoc($result)) )
		{
			$comments .= '<div class="comment col-md-10">';
			$comments .= '<h3>' . $cat['name'] . '</h3><span>' . $cat['mail'] .'</span>';
			$comments .= '<p>' . $cat['text'] . '</p>';
			
			$comments .= '</div>';
					
		}
		
	}
?>